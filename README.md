## Final Project

## Kelompok 6

- Muhammad Aldy Kurniawan
- Zainul Fuadi
- Maulana Habibullah

## Tema Project

Perpustakaan

## ERD

![Img 1](erd/erd-perpus.png)


## Template

- Ruang Admnin


## Packet

- Datatable
- DomPDF
- Laravel Auth
- Laravel Mix

## Email & Password Login

- Email : admin@admin.com
- Password : admin123


## Link

- Link Demo Aplikasi : https://drive.google.com/drive/folders/1fDusZhOvfaqzao31Ai-B19D0kYzck1jS?usp=sharing
- Link Deploy : https://perpuskita.gaoanon.com/