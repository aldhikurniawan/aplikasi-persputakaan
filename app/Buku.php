<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
    public function genre() {
        return $this->belongsTo('App\Genre');
    }
    public function penerbit() {
        return $this->belongsTo('App\Penerbit');
    }
    public function pengarang() {
        return $this->belongsTo('App\Pengarang');
    }
    public function pinjam() {
        return $this->hasMany('App\Pinjam');
    }
    public function kode() {
        return $this->hasOne('App\Kode');
    }

    protected $table = "bukus";
    protected $fillable = ["nama", "tahun", "deskripsi", "penerbit_id", "pengarang_id", "genre_id"];
}
