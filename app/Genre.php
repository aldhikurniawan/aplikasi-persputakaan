<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    public function buku() {
        return $this->hasMany('App\Genre');
    }
    protected $table = "genres";
    protected $fillable = ["nama"];
}
