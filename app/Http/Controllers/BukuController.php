<?php

namespace App\Http\Controllers;

use App\Buku;
use App\Genre;
use App\Penerbit;
use App\Pengarang;
use Illuminate\Http\Request;
use File;

class BukuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $buku = Buku::all();
        return view('buku.index', compact('buku'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pengarangs = Pengarang::all();
        $genres = Genre::all();
        $penerbits = Penerbit::all();
        return view('buku.create', compact('pengarangs', 'genres', 'penerbits'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'nama' => 'required',
            'tahun' => 'required',
            'deskripsi' => 'required',
            'penerbit_id' => 'required',
            'pengarang_id' => 'required',
            'genre_id' => 'required'
        ]);
        $input = $request->all(); 
        
        Buku::create($input);
        return redirect('/buku');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Buku  $buku
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $buku = Buku::find($id);
        return view('buku.show', compact('buku'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Buku  $buku
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $buku = Buku::find($id);
        $pengarangs = Pengarang::all();
        $genres = Genre::all();
        $penerbits = Penerbit::all();
        return view('buku.edit', compact('buku', 'pengarangs', 'genres', 'penerbits'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Buku  $buku
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Buku $buku)
    {
        $request->validate([
            'nama' => 'required',
            'tahun' => 'required',
            'deskripsi' => 'required',
            'penerbit_id' => 'required',
            'pengarang_id' => 'required',
            'genre_id' => 'required'
        ]);

        Buku::where('id', $buku->id)
        ->update([
            'nama' => $request->nama,
            'tahun' => $request->tahun,
            'deskripsi' => $request->deskripsi,
            'penerbit_id' => $request->penerbit_id,
            'pengarang_id' => $request->pengarang_id,
            'genre_id' => $request->genre_id 
        ]);
        $input = $request->all();

        return redirect('/buku');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Buku  $buku
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $buku = Buku::find($id);
        $buku->delete();
        return redirect('/buku');
    }
}
