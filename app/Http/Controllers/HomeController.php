<?php

namespace App\Http\Controllers;

use App\Pinjam;
use App\Peminjam;
use App\Buku;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $buku = Buku::sum('id');
        $terbaru = Buku::pluck('nama')->last();
        $transaksi = Pinjam::sum('id');
        $anggota = Peminjam::sum('id');
        return view('home', compact('buku', 'terbaru', 'transaksi', 'anggota'));
    }
}
