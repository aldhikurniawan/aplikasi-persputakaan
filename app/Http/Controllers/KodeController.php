<?php

namespace App\Http\Controllers;

use App\Buku;
use App\Kode;
use Illuminate\Http\Request;

class KodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kode = Kode::all();
        return view('kode.index', compact('kode'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bukus = Buku::all();
        return view('kode.create', compact('bukus'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'isbn' => 'required',
            'buku_id' => 'required'
        ]);
        
        $input = $request->all();

        Kode::create($input);
        return redirect('/kode');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Kode  $kode
     * @return \Illuminate\Http\Response
     */
    public function show(Kode $kode)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Kode  $kode
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kode = Kode::find($id);
        $bukus = Buku::all();
        return view('kode.edit', compact('kode', 'bukus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Kode  $kode
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Kode $kode)
    {
        $request->validate([
            'isbn' => 'required',
            'buku_id' => 'required'
        ]);
        Kode::where('id', $kode->id)
        ->update([
            'isbn' => $request->isbn,
            'buku_id' => $request->buku_id
        ]);
        $input = $request->all();

        return redirect('/kode');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Kode  $kode
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kode = Kode::find($id);
        $kode->delete();
        return redirect('/kode');
    }
}
