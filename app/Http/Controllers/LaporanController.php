<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pinjam;
use PDF;

class LaporanController extends Controller
{
    public function index(Request $request)
    {
        $pinjam = Pinjam::get();
        return view('laporan.index', compact('pinjam'));
    }

    public function searchByDate(Request $request)
    {
        $from = $request->from;
        $to = $request->to;
        $pinjam = Pinjam::where('tanggal_pinjam', '>=', $request->from)->where('tanggal_pinjam', '<=', $request->to)->get();
        $pdf = PDF::loadview('laporan.print', compact('pinjam', 'from', 'to'))->setpaper('A4', 'landscape');
        return $pdf->stream();
    }
}