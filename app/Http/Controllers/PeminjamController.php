<?php

namespace App\Http\Controllers;

use App\Peminjam;
use Illuminate\Http\Request;


class PeminjamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $peminjam = Peminjam::get();
        return view('peminjam.index', compact('peminjam'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('peminjam.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'jenis_kelamin' => 'required',
            'nomor_identitas' => 'required'
        ]);

        Peminjam::create([
            'nama' => $request->nama,
            'tempat_lahir' => $request->tempat_lahir,
            'tanggal_lahir' => $request->tanggal_lahir,
            'jenis_kelamin' => $request->jenis_kelamin,
            'nomor_identitas' => $request->nomor_identitas
        ]);

        return redirect('peminjam');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Peminjam  $peminjam
     * @return \Illuminate\Http\Response
     */
    public function show(Peminjam $peminjam)
    {
        return view('peminjam.show', compact('peminjam'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Peminjam  $peminjam
     * @return \Illuminate\Http\Response
     */
    public function edit(Peminjam $peminjam)
    {
        return view('peminjam.edit', compact('peminjam'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Peminjam  $peminjam
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Peminjam $peminjam)
    {
        
        $request->validate([
            'nama' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'jenis_kelamin' => 'required',
            'nomor_identitas' => 'required'
        ]);

        $peminjam = Peminjam::find($peminjam->id);
        $peminjam->nama = $request->nama;
        $peminjam->tempat_lahir = $request->tempat_lahir;
        $peminjam->tanggal_lahir = $request->tanggal_lahir;
        $peminjam->jenis_kelamin = $request->jenis_kelamin;
        $peminjam->nomor_identitas = $request->nomor_identitas;
        $peminjam->update();
        return redirect('/peminjam');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Peminjam  $peminjam
     * @return \Illuminate\Http\Response
     */
    public function destroy(Peminjam $peminjam)
    {
        $peminjam = Peminjam::find($peminjam->id);
        $peminjam->delete();
        return redirect('/peminjam');
    }
}
