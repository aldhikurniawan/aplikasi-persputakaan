<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kode extends Model
{
    public function buku() {
        return $this->belongsTo('App\Buku');
    }

    protected $table = "kodes";
    protected $fillable = ["isbn", "buku_id"];
}
