<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Peminjam extends Model
{
    public function pinjam() {
        return $this->hasMany('App\Pinjam', 'user_id', 'id');
    }
    public function delete() {
        $this->pinjam()->delete();
        return parent::delete();
    }

    protected $table = "peminjams";
    protected $fillable = ["nama", "tempat_lahir", "tanggal_lahir", "jenis_kelamin", "nomor_identitas"];
}
