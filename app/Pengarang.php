<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Pengarang extends Model
{
    public function buku() {
        return $this->HasMany('App\Buku');
    }
    public function delete() {
        $this->buku()->delete();
        return parent::delete();
    }

    protected $table = "pengarangs";
    protected $fillable = ["nama", "biografi", "jenis_kelamin", "tempat_lahir", "tanggal_lahir"];
}
