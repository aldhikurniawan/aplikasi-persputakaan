<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pinjam extends Model
{
    public function peminjam() {
        return $this->belongsTo('App\Peminjam', 'user_id', 'id');
    }
    public function buku() {
        return $this->belongsTo('App\Buku');
    }

    protected $table = "pinjams";
    protected $fillable = ["tanggal_pinjam", "tanggal_kembali", "status", "user_id", "buku_id"];
}
