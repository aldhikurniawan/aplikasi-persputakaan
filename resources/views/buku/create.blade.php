@extends('layouts.home')

@section('title', 'Tambah Buku')

@section('content')
<div class="card my-3">
    <div class="card-body">
        <form action="{{ url('buku') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-6">
                    <div class="form-group">
                        <label for="Nama">Nama Buku</label>
                        <input type="text" class="form-control" name="nama" id="nama" value="{{ old('nama')}}" placeholder="Masukkan Nama Buku">
                        @error('nama')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="mb-3">
                        <label for="deskripsi" name="deskripsi" id="deskripsi" class="form-label">Deskripsi Buku</label>
                        <textarea class="form-control" name="deskripsi" id="deskripsi" placeholder="Masukkan Deskripsi Buku" rows="3">{{ old('deskripsi')}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="tahun">Tahun Terbit</label>
                        <input type="text" class="form-control" name="tahun" id="tahun" value="{{ old('tahun')}}" placeholder="Masukkan Tahun Terbit">
                        @error('tahun')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label for="penerbit">Kode Penerbit</label>
                        <select class="form-control" name="penerbit_id">
                            <option selected="true" disabled="disabled">Pilih Penerbit</option>
                            @foreach ($penerbits as $penerbit) 
                                <option value="{{ $penerbit->id }}">{{ $penerbit->nama }}</option> 
                            @endforeach
                        </select>
                        @error('penerbit_id')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="pengarang">Kode Pengarang</label>
                        <select class="form-control" name="pengarang_id">
                            <option selected="true" disabled="disabled">Pilih Pengarang</option>
                            @foreach ($pengarangs as $pengarang) 
                                <option value="{{ $pengarang->id }}">{{ $pengarang->nama }}</option> 
                            @endforeach
                        </select>
                        @error('pengarang_id')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="genre">Kode Genre</label>
                        <select class="form-control" name="genre_id">
                            <option selected="true" disabled="disabled">Pilih Genre</option>
                            @foreach ($genres as $genre) 
                                <option value="{{ $genre->id }}">{{ $genre->nama }}</option> 
                            @endforeach
                        </select>
                        @error('genre_id')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-success">Tambah</button>
            <a href="{{ url('buku') }}" class="btn btn-secondary my-3">
                <span class="text">Kembali</span>
            </a>
        </form>
    </div>
</div>
@endsection