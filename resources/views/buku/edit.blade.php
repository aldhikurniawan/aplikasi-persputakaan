@extends('layouts.home')

@section('title', 'Edit Buku')

@section('content')
<div class="card">
    <div class="card-body">
        <h3 class="mb-3">"{{$buku->nama}}"</h3>
        <form action="{{ url('buku/'.$buku->id) }}" method="POST">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col-6">
                    <div class="form-group">
                        <label for="nama">Nama Buku</label>
                        <input type="text" class="form-control" name="nama" id="nama" value="{{ $buku->nama }}" placeholder="Masukkan Nama Buku">
                        @error('nama')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="mb-3">
                        <label for="deskripsi" name="deskripsi" id="deskripsi" class="form-label">Deskripsi Buku</label>
                        <textarea class="form-control" name="deskripsi" id="deskripsi" placeholder="Masukkan Deskripsi Buku" rows="3">{{ $buku->deskripsi }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="tahun">Tahun Terbit</label>
                        <input type="text" class="form-control" name="tahun" id="tahun" value="{{ $buku->tahun }}" placeholder="Masukkan Tahun Terbit">
                        @error('tahun')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label for="penerbit">Kode penerbit</label>
                        <select class="form-control" name="penerbit_id" value="{{ $buku->penerbit_id }}">
                            <option selected="true" disabled="disabled">
                                Pilih penerbit
                            </option> 
                            @foreach ($penerbits as $penerbit)
                                <option value="{{ $penerbit->id }} " @if($buku->penerbit_id == $penerbit->id) selected @endif> {{ $penerbit->nama }} 
                                </option>
                            @endforeach
                        </select>
                        @error('penerbit_id')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="pengarang">Kode Pengarang</label>
                        <select class="form-control" name="pengarang_id" value="{{ $buku->pengarang_id }}">
                            <option selected="true" disabled="disabled">
                                Pilih Pengarang
                            </option> 
                            @foreach ($pengarangs as $pengarang)
                                <option value="{{ $pengarang->id }} " @if($buku->pengarang_id == $pengarang->id) selected @endif> {{ $pengarang->nama }} 
                                </option>
                            @endforeach
                        </select>
                        @error('pengarang_id')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="genre">Kode genre</label>
                        <select class="form-control" name="genre_id" value="{{ $buku->genre_id }}">
                            <option selected="true" disabled="disabled">
                                Pilih genre
                            </option> 
                            @foreach ($genres as $genre)
                                <option value="{{ $genre->id }} " @if($buku->genre_id == $genre->id) selected @endif> {{ $genre->nama }} 
                                </option>
                            @endforeach
                        </select>
                        @error('pengarang_id')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-warning">Edit</button>
            <a href="{{ url('buku') }}" class="btn btn-secondary my-3">
                <span class="text">Kembali</span>
            </a>
        </form>
    </div>
</div>
@endsection