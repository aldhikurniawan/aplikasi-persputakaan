@extends('layouts.home')

@section('title', 'Detail Buku')

@section('content')
<style>
    th {
        max-width: 40px;
    }
</style>

<div class="card">
    <table class="table table-borderless">
        <tr>
            <th>Kode buku</th>
            <td>{{ $buku->kode_id }}</td>
        </tr>
        <tr>
            <th>Judul buku</th>
            <td>{{ $buku->nama }}</td>
        </tr>
        <tr>
            <th>Pengarang</th>
            <td>{{ $buku->pengarang->nama }}</td>
        </tr>
        <tr>
            <th>Deskripsi</th>
            <td>{{ $buku->deskripsi }}</td>
        </tr>
        <tr>
            <th>Penerbit</th>
            <td>{{ $buku->penerbit->nama }}</td>
        </tr>
        <tr>
            <th>Genre</th>
            <td>{{ $buku->genre->nama }}</td>
        </tr>
    </table>
</div>
<a href="{{ url('buku') }}" class="btn btn-icon-split btn-secondary btn-sm my-3">
    <span class="icon text-white-50">
        <i class="fas fa-arrow-left"></i>
    </span>
    <span class="text">Kembali</span>
</a>
@endsection