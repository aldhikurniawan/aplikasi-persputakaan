@extends('layouts.home')

@section('title', 'Tambah Genre')

@section('content')
<div class="card">
    <div class="card-body">
        <form action="{{ url('genre') }}" method="POST">
            @csrf
            <div class="form-group">
                <label for="Nama">Genre</label>
                <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama Genre">
                @error('title')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-success">Tambah</button>
            <a href="{{ url('genre') }}" class="btn btn-secondary my-3">
                <span class="text">Kembali</span>
            </a>
        </form>
    </div>
</div>
@endsection