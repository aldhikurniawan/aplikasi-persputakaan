@extends('layouts.home')

@section('title', 'Edit Genre')

@section('content')
<div class="card">
    <div class="card-body">
        <form action="{{ url('genre/'.$genre->id) }}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group" style="max-width: 18rem">
                <input type="text" class="form-control" name="nama" value="{{$genre->nama}}" id="nama" placeholder="Masukkan Nama">
                @error('title')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-warning">Edit</button>
            <a href="{{ url('genre') }}" class="btn btn-secondary my-3">
                <span class="text">Kembali</span>
            </a>
        </form>
    </div>
</div>
@endsection