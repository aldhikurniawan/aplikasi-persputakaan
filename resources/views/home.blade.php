@extends('layouts.home')

@section('title', 'Dashboard')

@section('content')

<style>
  .icon
  {
    color: #00a0e0 !important;
  }
</style>

<div class="row mb-3">
  <div class="col-xl-3 col-md-6 mb-4">
      <div class="card h-100">
      <div class="card-body">
          <div class="row align-items-center">
          <div class="col mr-2">
              <div class="text-xs font-weight-bold text-uppercase mb-1">Total Buku</div>
              <div class="mt-2 mb-0 text-muted text-xs">
              <div class="h5 mb-0 font-weight-bold text-gray-800">{{ number_format($buku) }}</div>
              </div>
          </div>
          <div class="col-auto">
              <i class="fas fa-book fa-2x icon"></i>
          </div>
          </div>
      </div>
      </div>
  </div>
  <div class="col-xl-3 col-md-6 mb-4">
      <div class="card h-100">
      <div class="card-body">
          <div class="row no-gutters align-items-center">
          <div class="col mr-2">
              <div class="text-xs font-weight-bold text-uppercase mb-1">Buku Terbaru</div>
              <div class="mt-2 mb-0 text-muted text-xs">
              <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $terbaru }}</div>
              </div>
          </div>
          <div class="col-auto">
              <i class="fas fa-book-open fa-2x icon"></i>
          </div>
          </div>
      </div>
      </div>
  </div>
  <div class="col-xl-3 col-md-6 mb-4">
      <div class="card h-100">
      <div class="card-body">
          <div class="row no-gutters align-items-center">
          <div class="col mr-2">
              <div class="text-xs font-weight-bold text-uppercase mb-1">Total Transaksi</div>
              <div class="mt-2 mb-0 text-muted text-xs">
              <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">{{ number_format($transaksi) }}</div>
              </div>
          </div>
          <div class="col-auto">
              <i class="fas fa-clipboard-list fa-2x icon"></i>
          </div>
          </div>
      </div>
      </div>
  </div>
  <div class="col-xl-3 col-md-6 mb-4">
      <div class="card h-100">
      <div class="card-body">
          <div class="row no-gutters align-items-center">
          <div class="col mr-2">
              <div class="text-xs font-weight-bold text-uppercase mb-1">Total Anggota</div>
              <div class="mt-2 mb-0 text-muted text-xs">
              <div class="h5 mb-0 font-weight-bold text-gray-800">{{ number_format($anggota) }}</div>
              </div>
          </div>
          <div class="col-auto">
              <i class="fas fa-user fa-2x icon"></i>
          </div>
          </div>
      </div>
      </div>
  </div>
  </div>
@endsection