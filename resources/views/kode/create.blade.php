@extends('layouts.home')

@section('title', 'Tambah Kode')

@section('content')
<div class="card">
    <div class="card-body">
        <form action="{{ url('kode') }}" method="POST" style="max-width: 500px" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="Nama">ISBN</label>
                <input type="text" class="form-control" name="isbn" id="isbn" placeholder="Masukkan Kode ISBN">
                @error('isbn')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="buku_id">Judul Buku</label>
                <select class="form-control" name="buku_id">
                    <option selected="true" disabled="disabled">Pilih Buku</option>
                    @foreach ($bukus as $buku) 
                        <option value="{{ $buku->id }}">{{ $buku->nama }}</option> 
                    @endforeach
                </select>
            </div>
            <button type="submit" class="btn btn-success">Tambah</button>
            <a href="{{ url('kode') }}" class="btn btn-secondary my-3">
                <span class="text">Kembali</span>
            </a>
        </form>
    </div>
</div>
@endsection