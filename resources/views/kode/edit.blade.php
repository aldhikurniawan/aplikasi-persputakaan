@extends('layouts.home')

@section('title', 'Edit Kode')

@section('content')
<div class="card">
    <div class="card-body">
        <form action="{{ url('kode/'.$kode->id) }}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group" style="max-width: 18rem">
                <input type="text" class="form-control" name="nama" value="{{$kode->isbn}}" id="nama" placeholder="Masukkan Nama">
                @error('title')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-warning">Edit</button>
        </form>
    </div>
</div>
@endsection