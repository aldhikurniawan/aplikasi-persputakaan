@extends('layouts.home')

@section('title', 'Laporan Buku')

@section('content')

<div class="card shadow mb-4">
    <div class="card-body" style="max-width: 500px">
      <form action="{{ url('laporan') }}" method="post">
        @csrf
        <div class="form-group">
          <label for="tanggal">Dari Tanggal</label>
          <input type="date" class="form-control @error('from') is-invalid @enderror" id="from" name="from" value="{{ date('Y-m-d') }}">&nbsp;
          @error('from')<div class="invalid-feedback">{{ $message }}</div>@enderror
        </div>
        <div class="form-group">
          <label>Sampai Tanggal</label>
          <input type="date" class="form-control @error('to') is-invalid @enderror" id="to" name="to" value="{{ date('Y-m-d') }}">&nbsp;
          @error('to')<div class="invalid-feedback">{{ $message }}</div>@enderror
        </div>
        <button type="submit" class="btn btn-icon-split btn-primary" value="view" target="blank_">
          <span class="text">Tampilkan</span>
        </button>
      </form>
    </div>
</div>

@endsection