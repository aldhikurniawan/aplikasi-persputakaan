<style>
    *
    {
        font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
    }
    table 
    {
      border-collapse: collapse;
      width: 100%;
      font-size: 11px;
    }
    thead th
    {
        background-color: #f3f3f3;
    }
    td, th 
    {
      border: 1px solid #dddddd;
      text-align: left;
      padding: 8px;
    }
    th
    {
        text-align: center;
    }
    .heading
    {
        text-align: center;
    }
    .heading p
    {
        margin-bottom: 0px;
        font-weight: bold;
        font-size: 15px;
    }
    .nowrap
    {
        white-space: nowrap;
    }
    .center
    {
        text-align: center;
    }
    </style>
  
    <div class="heading">
      <p>Laporan Transaksi<p>
      <p>Periode Tanggal {{ $from }} s/d {{ $to }}</p>
      <br>
    </div>
  
    <table class="table table-bordered middle md-3" id="example">
      <thead>
      <tr>
          <th scope="col">No</th>
          <th scope="col">Nama Peminjam</th>
          <th scope="col">Judul Buku</th>
          <th scope="col">Tanggal Pinjam</th>
          <th scope="col">Tanggal Kembali</th>
          <th scope="col">Status</th>
      </tr>
      </thead>
      <tbody>
      @forelse ($pinjam as $item)
      <tr>
          <th scope="row">{{ $loop->iteration }}</th>
          <td>{{ $item->peminjam->nama }}</td>
          <td>{{ $item->buku->nama }}</td>
          <td>{{ $item->tanggal_pinjam }}</td>
          <td>{{ $item->tanggal_kembali }}</td>
          <td>
          @if ($item->status == 'dipinjam')
              <span class="badge badge-danger">Dipinjam</span>
          @else
              <span class="badge badge-danger">Dikembalikan</span>
          @endif
          </td>
      </tr>
      @empty
      <tr>
          <td colspan="6">Data kosong</td>
      </tr>
      @endforelse
      </tbody>
  </table>