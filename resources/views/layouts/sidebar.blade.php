<style>
    .warna{
        background-color: white !important;
    }
    .tulisan{
        color: #00a0e0 !important;
    }
</style>

<ul class="navbar-nav sidebar sidebar-light accordion" id="accordionSidebar">
    <a class="sidebar-brand d-flex align-items-center justify-content-center warna" href="index.html">
      <div class="sidebar-brand-icon">
        <img  src="{{ asset('img/book.png') }}">
      </div>
      <div class=" tulisan sidebar-brand-text mx-3">PERPUSKITA</div>
    </a>
    <hr class="sidebar-divider my-0">
    <li class="nav-item">
      <a class="nav-link" href="{{ url('home') }}">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span></a>
    </li>
    <hr class="sidebar-divider">
    <div class="sidebar-heading">
      Menus
    </div>
    <li class="nav-item">
      <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseBootstrap"
        aria-expanded="true" aria-controls="collapseBootstrap">
        <i class="far fa-fw fa-window-maximize"></i>
        <span>Master Data</span>
      </a>
      <div id="collapseBootstrap" class="collapse" aria-labelledby="headingBootstrap" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
          <h6 class="collapse-header"></h6>
          <a class="collapse-item" href="{{ url('kode') }}">Kode Buku</a>
          <a class="collapse-item" href="{{ url('genre') }}">Genre</a>
          <a class="collapse-item" href="{{ url('penerbit') }}">Penerbit</a>
          <a class="collapse-item" href="{{ url('pengarang') }}">Pengarang</a>
        </div>
      </div>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{ url('buku') }}">
        <i class="fas fa-fw fa-book"></i>
        <span>Buku</span>
      </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{ url('transaksi') }}">
          <i class="fas fa-fw fa-clipboard-list"></i>
          <span>Transaksi</span>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{ url('peminjam') }}">
          <i class="fas fa-fw fa-user"></i>
          <span>Data Peminjam</span>
        </a>
    </li>
    <li class="nav-item">
      <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTable" aria-expanded="true"
        aria-controls="collapseTable">
        <i class="fas fa-fw fa-table"></i>
        <span>Laporan</span>
      </a>
      <div id="collapseTable" class="collapse" aria-labelledby="headingTable" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
          <h6 class="collapse-header">Laporan</h6>
          <a class="collapse-item" href="{{ url('laporan') }}">Laporan Pinjaman</a>
          {{-- <a class="collapse-item" href="{{ url('buku') }}">Laporan Buku</a> --}}
        </div>
      </div>
    </li>
  </ul>