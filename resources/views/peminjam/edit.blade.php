@extends('layouts.home')

@section('title', 'Edit Peminjam')
   
@section('content')

<div class="card shadow mb-4">
    <div class="card"> 
    <form action="{{ url('peminjam/'.$peminjam->id) }}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <div class="col">
        <label>Nama</label>
        <input type="text" class="form-control" name="nama" placeholder="Masukkan Nama" value="{{$peminjam->nama}}">
        @error('nama')<div class="alert alert-danger">{{ $message }}</div>@enderror
        </div>
    </div>
    <div class="form-group">
        <div class="col">
        <label>Tempat Lahir</label>
        <input type="text" class="form-control" name="tempat_lahir" placeholder="Masukkan Tempat Lahir" value="{{ $peminjam->tempat_lahir }}">
        @error('tempat_lahir')<div class="alert alert-danger">{{ $message }}</div>@enderror
        </div>
    </div>
    <div class="form-group">
        <div class="col">
        <label>Tanggal Lahir</label>
        <input type="date" class="form-control" name="tanggal_lahir" placeholder="Masukkan Tanggal Lahir" value="{{ $peminjam->tanggal_lahir }}">
        @error('tanggal_lahir')<div class="alert alert-danger">{{ $message }}</div>@enderror
        </div>
    </div>
    <div class="form-group">
        <div class="col">
        <label>Jenis Kelamin</label>
        <select name="jenis_kelamin" class="form-control">
              <option value="" disabled >Pilih Jenis Kelamin</option>
              <option value="laki-laki" @if($peminjam->jenis_kelamin == 'laki-laki') selected @endif>Laki-laki</option>
              <option value="perempuan" @if($peminjam->jenis_kelamin == 'perempuan') selected @endif>Perempuan</option>
        </select>
        </div>
    </div>
    <div class="form-group">
        <div class="col">
        <label>Nomor Identitas</label>
        <input type="text" class="form-control" name="nomor_identitas" placeholder="Masukkan Nomor Identitas" value="{{ $peminjam->nomor_identitas }}">
        @error('nomor_identitas')<div class="alert alert-danger">{{ $message }}</div>@enderror</div>
    </div>
    <button type="submit" class="btn btn-success ml-3">
        <span class="text">Simpan</span>
    </button>
    <a href="{{ url('peminjam') }}" class="btn btn-secondary my-3">
        <span class="text">Kembali</span>
    </a>
    </form>
    </div>
</div>

@endsection