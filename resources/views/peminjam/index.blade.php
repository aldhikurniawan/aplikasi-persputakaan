@extends('layouts.home')

@section('title', 'Data Peminjam')
   
@section('content')


<a href="{{ url('peminjam/create') }}" class="btn btn-success btn-sm btn-icon-split btn-sm mb-3">
    <span class="icon text-white-50">
        <i class="fas fa-plus" style="color: white"></i>
    </span>
    <span class="text">Tambah</span>
  </a>
<div class="card">
    <div class="card-body">
        <table id="example" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Jenis Kelamin</th>
                <th scope="col">Nomor Identitas</th>
                <th scope="col">Aksi</th>
            </tr>
            </thead>
            <tbody>
                @foreach($peminjam as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->nama}}</td>
                        <td>{{$value->jenis_kelamin}}</td>
                        <td>{{$value->nomor_identitas}}</td>
                        <td>
                            <a href="{{ url('peminjam/'.$value->id) }}" class="btn btn-info btn-sm">
                                <i class="fas fa-eye"></i>
                            </a>
                            <a href="{{ url('peminjam/'.$value->id).'/edit' }}" class="btn btn-warning btn-sm">
                                <i class="fas fa-edit" style="color: white"></i>
                            </a>
                            <form action="{{ url('peminjam/'.$value->id) }}" method="POST" class="d-inline">
                                @method('delete')
                                @csrf
                                <button class="btn btn-danger btn-sm">
                                    <i class="fas fa-trash"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach            
            </tbody>
        </table>
    </div>
</div>
    
@endsection

@push('scripts')
<script>
    $(document).ready(function() {
    $('#example').DataTable();
} );
</script>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap4.min.js"></script>
@endpush