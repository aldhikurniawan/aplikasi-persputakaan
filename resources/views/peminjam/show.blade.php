@extends('layouts.home')

@section('title', 'Detail Peminjam')

@section('content')

<div class="card">
    <table class="table">
        <tr>
            <th>No Identitas</th>
            <td>{{ $peminjam->nomor_identitas }}</td>
        </tr>
        <tr>
            <th>Nama</th>
            <td>{{ $peminjam->nama }}</td>
        </tr>
        <tr>
            <th>Jenis Kelamin</th>
            <td>{{ $peminjam->jenis_kelamin }}</td>
        </tr>
        <tr>
            <th>TTL</th>
            <td>{{ $peminjam->tempat_lahir }}, {{ $peminjam->tanggal_lahir }}</td>
        </tr>
    </table>
</div>
<a href="{{ url('peminjam') }}" class="btn btn-icon-split btn-secondary btn-sm my-3">
    <span class="icon text-white-50">
        <i class="fas fa-arrow-left"></i>
    </span>
    <span class="text">Kembali</span>
</a>


@endsection