@extends('layouts.home')

@section('title', 'Tambah Penerbit')

@section('content')

<div class="card shadow mb-4">
    <div class="card"> 
    <form action="{{ url('penerbit') }}" method="POST">
    @csrf
    <div class="form-group">
        <div class="col">
        <label>Nama</label>
        <input type="text" class="form-control" name="nama" placeholder="Masukkan Nama" value="{{ old('nama')}}">
        @error('nama')<div class="alert alert-danger">{{ $message }}</div>@enderror
        </div>
    </div>
    <button type="submit" class="btn btn-success ml-3">
        <span class="text">Tambah</span>
    </button>
    <a href="{{ url('penerbit') }}" class="btn btn-secondary my-3">
        <span class="text">Kembali</span>
    </a>
    </div>
</div>

@endsection