@extends('layouts.home')

@section('title', 'Tambah Pengarang')

@section('content')

<div class="card shadow mb-4">
    <div class="card"> 
    <form action="{{ url('pengarang/'.$pengarang->id) }}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <div class="col">
        <label>Nama</label>
        <input type="text" class="form-control" name="nama" placeholder="Masukkan Nama" value="{{ old('nama') }}">
        @error('nama')<div class="alert alert-danger">{{ $message }}</div>@enderror
        </div>
    </div>
    <div class="form-group">
        <div class="col">
        <label>Biografi</label> <br>
        <textarea name="biografi" cols="124">{{ old('biografi') }}</textarea>
        @error('biografi')<div class="alert alert-danger">{{ $message }}</div>@enderror
        </div>
    </div>
    <div class="form-group">
        <div class="col">
        <label>Jenis Kelamin</label>
            <select name="jenis_kelamin" class="form-control">
                <option value="" disabled >Pilih Jenis Kelamin</option>
                <option value="laki-laki" @if($pengarang->jenis_kelamin == 'laki-laki') selected @endif>Laki-laki</option>
                <option value="perempuan" @if($pengarang->jenis_kelamin == 'perempuan') selected @endif>Perempuan</option>
          </select>
        </div>
    </div>
    <div class="form-group">
        <div class="col">
        <label>Tempat Lahir</label>
        <input type="text" class="form-control" name="tempat_lahir" placeholder="Masukkan Tempat Lahir" value="{{ old('tempat_lahir') }}">
        @error('tempat_lahir')<div class="alert alert-danger">{{ $message }}</div>@enderror
        </div>
    </div>
    <div class="form-group">
        <div class="col">
        <label>Tanggal Lahir</label>
        <input type="date" class="form-control" name="tanggal_lahir" placeholder="Masukkan Tanggal Lahir" value="{{ old('tanggal_lahir') }}">
        @error('tanggal_lahir')<div class="alert alert-danger">{{ $message }}</div>@enderror
        </div>
    </div>
    <button type="submit" class="btn btn-warning btn-icon-split btn ml-3">
        <span class="icon text-white-50">
            <i class="fas fa-edit"></i>
        </span>
        <span class="text">Edit</span>
    </button>
    <a href="{{ url('pengarang') }}" class="btn btn-secondary my-3">
        <span class="text">Kembali</span>
    </a>
    </form>
    </div>
</div>


@endsection