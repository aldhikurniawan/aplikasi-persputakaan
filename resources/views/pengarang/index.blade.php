@extends('layouts.home')

@section('title', 'Data Pengarang')

@section('content')
<a href="{{ url('pengarang/create') }}" class="btn btn-success btn-icon-split btn-sm mb-3">
    <span class="icon text-white-50">
        <i class="fas fa-plus" style="color: white"></i>
    </span>
    <span class="text">Tambah</span>
</a>
<div class="card">
    <div class="card-body">
        <table id="example" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Pengarang</th>
                    <th>Jenis Kelamin</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($pengarang as $key=>$value)
                    <tr>
                        <td class="col-1">{{ $key + 1 }}</td>
                        <td class="col-4">{{ $value->nama }}</td>
                        <td class="col-4">{{ $value->jenis_kelamin }}</td>
                        <td class="col-2">
                            <a href="{{ url('pengarang/'.$value->id).'/edit' }}" class="btn btn-warning btn-sm">
                                <i class="fas fa-edit" style="color: white"></i>
                            </a>
                            <form action="{{ url('pengarang/'.$value->id) }}" method="POST" class="d-inline">
                                @method('delete')
                                @csrf
                                <button class="btn btn-danger btn-sm">
                                    <i class="fas fa-trash"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="4">No Data</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>
@endsection

@push('scripts')
<script>
    $(document).ready(function() {
    $('#example').DataTable();
} );
</script>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap4.min.js"></script>
@endpush