@extends('layouts.home')

@section('title', 'Detail Peminjam')

@section('content')

<div class="card">
    <table class="table">
        <tr>
            <th>Nama</th>
            <td>{{ $pengarang->nama }}</td>
        </tr>
        <tr>
            <th>Biografi</th>
            <td>{{ $pengarang->biografi }}</td>
        </tr>
        <tr>
            <th>Jenis Kelamin</th>
            <td>{{ $pengarang->jenis_kelamin }}</td>
        </tr>
        <tr>
            <th>TTL</th>
            <td>{{ $pengarang->tempat_lahir }}, {{ $pengarang->tanggal_lahir }}</td>
        </tr>
    </table>
</div>
<a href="{{ url('pengarang') }}" class="btn btn-icon-split btn-secondary btn-sm my-3">
    <span class="icon text-white-50">
        <i class="fas fa-arrow-left"></i>
    </span>
    <span class="text">Kembali</span>
</a>


@endsection