@extends('layouts.home')

@section('title', 'Tambah Transaksi Peminjaman')

@section('content')
<div class="card my-3">
    <div class="card-body">
        <form action="{{ url('transaksi') }}" method="POST" enctype="multipart/form-data" style="max-width: 500px">
            @csrf
            <div class="form-group">
                <label for="peminjam">Anggota Peminjam</label>
                <select class="form-control" name="user_id">
                    <option selected="true" disabled="disabled">Pilih anggota</option>
                    @foreach ($peminjams as $peminjam) 
                        <option value="{{ $peminjam->id }}">{{ $peminjam->nama }}</option> 
                    @endforeach
                </select>
                @error('user_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="buku">Pilih Buku</label>
                <select class="form-control" name="buku_id">
                    <option selected="true" disabled="disabled">Pilih buku</option>
                    @foreach ($bukus as $buku) 
                        <option value="{{ $buku->id }}">{{ $buku->nama }}</option> 
                    @endforeach
                </select>
                @error('buku_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="tanggal_pinjam">Tanggal Pinjam Buku</label>
                <input type="date" class="form-control" name="tanggal_pinjam" id="tanggal_pinjam" value="{{ old('tanggal_pinjam')}}" placeholder="Masukkan tanggal pinjam">
                @error('tanggal_pinjam')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="tanggal_kembali">Tanggal Kembali Buku</label>
                <input type="date" class="form-control" name="tanggal_kembali" id="tanggal_kembali" value="{{ old('tanggal_kembali')}}" placeholder="Masukkan tanggal mengembalikan buku">
                @error('tanggal_kembali')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Status Meminjam</label>
                <select name="status" id="status" class="form-control">
                    <option selected="true" disabled="disable">Pilih status</option>
                    <option value="dipinjam">Dipinjam</option>
                    <option value="dikembalikan">Dikembalikan</option>
                </select>
            </div>
            <button type="submit" class="btn btn-success">Tambah</button>
            <a href="{{ url('transaksi') }}" class="btn btn-secondary my-3">
                <span class="text">Kembali</span>
            </a>
        </form>
    </div>
</div>
@endsection