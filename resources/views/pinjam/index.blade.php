@extends('layouts.home')

@section('title', 'Data Transaksi Peminjaman')

@section('content')
<a href="{{ url('transaksi/create') }}" class="btn btn-success btn-icon-split btn-sm mb-3">
    <span class="icon text-white-50">
        <i class="fas fa-plus" style="color: white"></i>
    </span>
    <span class="text">Tambah</span>
</a>
<div class="card">
    <div class="card-body">
        <table id="example" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Peminjam</th>
                    <th>Buku</th>
                    <th>Tanggal Pinjam</th>
                    <th>Tanggal Kembali</th>
                    <th>Status</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($pinjam as $key=>$value)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $value->peminjam->nama }}</td>
                        <td>{{ $value->buku->nama }}</td>
                        <td>{{ $value->tanggal_pinjam }}</td>
                        <td>{{ $value->tanggal_kembali }}</td>
                        <td>
                            @if ($value->status == 'dipinjam')
                                <h5><span class="badge bg-success" style="color: white; font-size: 1.1rem">Dipinjam</span></h5>
                            @else
                                <h5><span class="badge bg-info" style="color: white; font-size: 1.1rem"">Dikembalikan</span></h5>
                            @endif
                        </td>
                        <td>
                            <a href="{{ url('transaksi/'.$value->id).'/edit' }}" class="btn btn-warning btn-sm">
                                <i class="fas fa-edit" style="color: white"></i>
                            </a>
                            <form action="{{ url('transaksi/'.$value->id) }}" method="POST" class="d-inline">
                                @method('delete')
                                @csrf
                                <button class="btn btn-danger btn-sm">
                                    <i class="fas fa-trash"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

@push('scripts')
<script>
    $(document).ready(function() {
    $('#example').DataTable();
} );
</script>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap4.min.js"></script>
@endpush