<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::group(['middleware' => 'auth'], function(){
    Route::resource('peminjam', 'PeminjamController');
    Route::resource('pengarang', 'PengarangController');
    Route::resource('buku', 'BukuController');
    Route::resource('genre', 'GenreController');
    Route::resource('laporan', 'LaporanController');
    Route::resource('penerbit', 'PenerbitController');
    Route::resource('pengarang', 'PengarangController');
    Route::resource('transaksi', 'PinjamController');
    Route::resource('kode', 'KodeController');
    Route::get('/laporan', 'LaporanController@index');
    Route::post('/laporan', 'LaporanController@searchByDate');
});

Route::get('/home', 'HomeController@index')->name('home');